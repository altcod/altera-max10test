
module sample_vh9x6_counter(rsted, clk1, clk2, vd, hd, pixels);
	input rsted, clk1, clk2;
	output vd, hd, pixels;
	/* 
		clk1 is 1/4 period ahead of clk2. everything sync to rising-edge of clk2, 
		except HD sync to rising-edge of clk1. a line will be linelen+1 clock long, 
		including a HD-rising at clk1-rising-edge of 1'st clock (base 0), 
		and HD falling at clk2 linelen'th clock. in the end, there is an extra 
		trailing 0x3ff pixcnt.
	 */
	
	reg rstd_state, rstd_last; // used to detect a rstd falling-edge
	reg vd, hd;
	reg [7:0] pixels;
	reg [19:0] counts;
	reg [29:0] linecnt;
	reg [29:0] pixcnt;
	reg linestart_detected;
	
	initial begin
		rstd_state = 1'b0;
		vd = 1'b0;
		hd = 1'b0;
		pixels = 8'b0;
		counts = 20'b0;
		linecnt = 30'b0;
		pixcnt = 30'b0; // pixcnt 3ff signals a start of line
		linestart_detected = 1'b0; // rising of clk1 finds pixcnt 3ff
	end
	
	parameter linelen = 9;  // 9 pixels per line
	parameter linehead = 3; // 3 pixels for line sync at the beginning
	parameter linetail = 3; // 3 pixels for line sync at the end
	parameter linetotal = linehead + linelen + linetail;
	parameter vlen = 6;  // 6 lines per frame
	parameter vhead = 2; // 2 lines for vertical sync at the top
	parameter vtail = 2; // 2 lines for vertical sync at the end
	parameter vtotal = vhead + vlen + vtail;
	
	always @(posedge clk1)
	begin
		if ( rstd_state == 1'b1 ) begin
			if ( pixcnt == 30'h3fffffff && vd == 1'b0 )
				linestart_detected <= 1'b1;
			if (linestart_detected && pixcnt >= linehead && pixcnt != 30'h3fffffff ) begin
				hd <= 1'b1;
				linestart_detected <= 1'b0;
			end else if (pixcnt != 30'h3fffffff && pixcnt >= linehead + linelen ) begin
				hd <= 1'b0;
			end
		end
	end
	
	always @(posedge clk2)
	begin
		rstd_last <= rsted;
		if ( rstd_state == 1'b0 ) begin
			if ( rsted == 1'b1 && rstd_last == 1'b0 )
				rstd_state <= 1'b1;
			else 
				rstd_state <= 1'b0;
			vd <= 1'b1;
			pixels = 8'b0;
			counts <= 20'b0;
			linecnt <= 30'b0;
			pixcnt <= 30'b0;
		end else begin
			if ( linecnt >= vhead && linecnt < vhead + vlen ) begin
				if ( hd == 1'b1 ) begin
					pixels[7:0] <= counts[7:0];
					counts <= counts + 1;
					pixcnt <= pixcnt + 1;
				end else if (pixcnt >= linetotal && pixcnt != 30'h3fffffff) begin
					pixcnt <= 30'h3fffffff;
					linecnt <= linecnt + 1;
					if ( linecnt >= vhead + vlen - 1 )
						vd <= 1'b1; // next line on next clock will be in sync
				end else begin
					pixcnt <= pixcnt + 1;
				end
			end else begin
				if (linecnt == vhead - 1) begin
					vd <= 1'b0;
					pixcnt <= 30'h3fffffff; // signal a start of line
				end else
					vd <= 1'b1;
				if (linecnt < vtotal - 1 )
					linecnt <= linecnt + 1;
				else
					linecnt <= 30'b0;
			end
		end
		
	end
endmodule
